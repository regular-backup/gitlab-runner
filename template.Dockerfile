ARG PACKAGE
ARG BASE_IMAGE

FROM ${BASE_IMAGE}

RUN apk add ${PACKAGE} && rm -rf /var/cache/apk/*
