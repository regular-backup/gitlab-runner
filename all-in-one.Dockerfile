ARG BASE_IMAGE

FROM ${BASE_IMAGE}

RUN apk add zip postgresql-client mysql-client mongodb-tools && rm -rf /var/cache/apk/*
