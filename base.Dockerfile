FROM gitlab/gitlab-runner:alpine-v14.3.0-rc1

RUN apk update && apk add bash 

RUN wget https://dl.min.io/client/mc/release/linux-amd64/mc -O /usr/local/bin/mc && chmod +x -R /usr/local/bin
